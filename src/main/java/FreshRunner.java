import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

public class FreshRunner {

    public static void main(String[] args) throws InterruptedException, IOException {

        Fresh fresh = new Fresh("", "", true);
        fresh.run(60);

    }

}
