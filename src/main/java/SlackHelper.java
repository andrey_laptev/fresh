import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

public class SlackHelper {

    private static final String WEBHOOK_URL =
            "https://hooks.slack.com/services/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";

    private SlackHelper() {}

    public static String send(String text) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(WEBHOOK_URL);
            StringEntity params = new StringEntity("{\"text\":\"" + text + "\"}");
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            return response.getEntity().getContent().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}