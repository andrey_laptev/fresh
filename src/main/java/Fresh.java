import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

public class Fresh {

    private String user;
    private String password;
    private boolean otp;

    private WebDriver wd;

    private String startUrl = "https://www.amazon.com";
    private String cartUrl = "https://www.amazon.com/gp/cart/view.html?ref_=nav_cart";

    private By signIn = By.xpath("(//span[text()='Sign in'])[3]");
    private By email = By.xpath("//input[@type='email']");
    private By continueButton = By.xpath("//input[@id='continue']");
    private By passwordField = By.xpath("//input[@type='password']");
    private By signInSubmit = By.xpath("//input[@id='signInSubmit']");
    private By proceedToCheckoutOne = By.xpath("//input[@value='Proceed to checkout']");
    private By proceedToCheckoutTwo = By.xpath("//a[@name='proceedToCheckout']");
    private By daySlots = By.xpath("//div[@class='ufss-date-select-toggle-text-availability']");
    private By dayToggles = By.xpath("//li[@class='ufss-date-select-toggle-container']");
    private By timeslotButtons = By.xpath("//li[@class='ufss-slot-container']//button");
    private By timeslotContinue = By.xpath("//span[contains(@class,'ufss-overview-continue-button')]//input");
    private By paymentContinue = By.xpath("//input[@id='continue-top']");
    private By missingGoodsAlert = By.xpath("//h4[contains(text(),'we are unable to fulfill your entire order')]");
    private By missingGoodsContinue = By.xpath("//input[@name='continue-bottom']");
    private By spinnerA = By.xpath("//div[@id='first-pipeline-load-page-spinner-blocker']");
    private By spinnerB = By.xpath("//div[@class='loading-spinner']");


    public Fresh(String user, String password, boolean otp) {
        this.user = user;
        this.password = password;
        this.otp = otp;

        System.setProperty("webdriver.chrome.driver","src/test/chromedriver/chromedriver.exe");
        wd = new ChromeDriver();
        wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    public void run(int pauseInSec) throws InterruptedException, IOException {
        wd.get(startUrl);
        Thread.sleep(2000);
        wd.findElement(signIn).click();
        wd.findElement(email).sendKeys(user);
        wd.findElement(continueButton).click();
        wd.findElement(passwordField).sendKeys(password);
        wd.findElement(signInSubmit).click();
        Thread.sleep(1000);
        if (otp) {
            Thread.sleep(30000);
        }

        wd.get(cartUrl);
        wd.findElement(proceedToCheckoutOne).click();
        wd.findElement(proceedToCheckoutTwo).click();

        int count = 1;
        while (true) {
            System.out.println(count);
            for (int i=0; i<3; i++) {
                if (!wd.findElements(daySlots).get(i).getText().contains("Not available")) {
                    SlackHelper.send("fresh!");
                    System.out.println("gotcha! day " + (i + 1));
                    waitForSpinner();
                    Files.write(Paths.get("src/main/" + getTimestamp() + "-available.html"), wd.getPageSource().getBytes());

                    wd.findElements(dayToggles).get(i).click();
                    waitForSpinner();
                    Files.write(Paths.get("src/main/" + getTimestamp() + "-day.html"), wd.getPageSource().getBytes());

                    scrollToMidScreen(timeslotButtons);
                    wd.findElement(timeslotButtons).click();
                    waitForSpinner();
                    Files.write(Paths.get("src/main/" + getTimestamp() + "-slot.html"), wd.getPageSource().getBytes());

                    wd.findElement(timeslotContinue).click();
                    waitForSpinner();
                    Files.write(Paths.get("src/main/" + getTimestamp() + "-continue.html"), wd.getPageSource().getBytes());

                    wd.findElement(paymentContinue).click();
                    waitForSpinner();
                    Files.write(Paths.get("src/main/" + getTimestamp() + "-payment.html"), wd.getPageSource().getBytes());

                    if (wd.findElements(missingGoodsAlert).size() != 0) {
                        wd.findElement(missingGoodsContinue).click();
                        waitForSpinner();
                        Thread.sleep(20000);
                        Files.write(Paths.get("src/main/" + getTimestamp() + "-missing.html"), wd.getPageSource().getBytes());
                    }


                    Thread.sleep(120000);

                    break;
                }
            }
            Thread.sleep(pauseInSec * 1000);
            wd.get(wd.getCurrentUrl());
            count++;
        }
    }

    private String getTimestamp() {
        return new Timestamp(System.currentTimeMillis()).toString().replaceAll(":","-");
    }

    private void scrollToMidScreen(By elementLocator) {
        String scrollElementIntoMiddle =
                "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                        + "var elementTop = arguments[0].getBoundingClientRect().top;"
                        + "window.scrollBy(0, elementTop-(viewPortHeight/2));";

        ((JavascriptExecutor) wd).executeScript(scrollElementIntoMiddle, wd.findElement(elementLocator));
    }

    private void waitForSpinner() throws InterruptedException {
        wd.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        if ((wd.findElements(spinnerA).size() != 0) || (wd.findElements(spinnerB).size() != 0)) {
            for (int i=0; i<240; i++) {
                if ((wd.findElements(spinnerA).size() == 0) && (wd.findElements(spinnerA).size() == 0)) {
                    break;
                }
                else {
                    Thread.sleep(500);
                }
            }
        }
        wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

}
